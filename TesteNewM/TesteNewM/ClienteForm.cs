﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using TesteNewM.Mapeamento;
using TesteNewM.Negocios;

namespace TesteNewM
{
    public partial class CadastroDeClientesForm : Form
    {
        private bool novoCliente = true;
        private int idCliente = 0;


        public CadastroDeClientesForm()
        {
            novoCliente = true;

            InitializeComponent();

            cpfMaskedTextBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            celularMaskedTextBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

        }

        public CadastroDeClientesForm(int id)
        {
            novoCliente = false;
            idCliente = id;

            InitializeComponent();

            cpfMaskedTextBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            celularMaskedTextBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

            Cliente cliente = new ClienteDAO().procurarPorID(id);
            nomeTextBox.Text = cliente.Nome;
            nascimentoMaskedTextBox.Text = Convert.ToString(cliente.Nascimento); 
            cpfMaskedTextBox.Text = cliente.CPF;
            celularMaskedTextBox.Text = cliente.Celular;
            emailTextBox.Text = cliente.Email;
            enderecoTextBox.Text = cliente.Endereco;
            observacaoTextBox.Text = cliente.Observacao;

        }

        private void salvarButton_Click(object sender, EventArgs e)
        {
            
            if (ValidarCampos())
            {

                if (IsCpf(cpfMaskedTextBox.Text))
                {
                    if (novoCliente)
                    {
                        Cliente cliente = new Cliente();
                        
                        cliente.Nome = RemoverEspacos(nomeTextBox.Text);
                        cliente.Nascimento = Convert.ToDateTime(nascimentoMaskedTextBox.Text);
                        cliente.CPF = cpfMaskedTextBox.Text;
                        cliente.Celular = celularMaskedTextBox.Text;
                        cliente.Email = RemoverEspacos(emailTextBox.Text);
                        cliente.Endereco = RemoverEspacos(enderecoTextBox.Text);
                        cliente.Observacao = observacaoTextBox.Text;


                        var criado = new ClienteDAO().Criar(cliente);

                        if (criado)
                        {
                            MessageBox.Show("Cadastro de cliente efetuado", "Novo de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Cadastro de cliente não pode ser efetuado", "Novo de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }


                    }
                    else
                    {
                        Cliente cliente = new Cliente();

                        cliente.ClientesID = idCliente;
                        cliente.Nome = RemoverEspacos(nomeTextBox.Text);
                        cliente.Nascimento = Convert.ToDateTime(nascimentoMaskedTextBox.Text);
                        cliente.CPF = cpfMaskedTextBox.Text;
                        cliente.Celular = celularMaskedTextBox.Text;
                        cliente.Email = RemoverEspacos(emailTextBox.Text);
                        cliente.Endereco = RemoverEspacos(enderecoTextBox.Text);
                        cliente.Observacao = observacaoTextBox.Text;


                        var atualizado = new ClienteDAO().Atualizar(cliente);

                        if (atualizado)
                        {
                            MessageBox.Show("Cadastro atualizado!", "Atualização de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Atualização de cadastro não efetuada!", "Atualização de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        }


                    }
                }
                else
                    MessageBox.Show("CPF inválido!", "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            else
                MessageBox.Show("Preencha os campos obrigatórios vazios", "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            

        }
        

        private void nomeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (!(char.IsLetter(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
                MessageBox.Show("!", "Caractere não aceito", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidarCampos()
        {
            if ((nomeTextBox.Text == "") || (nascimentoMaskedTextBox.Text.Length < 8) || (cpfMaskedTextBox.Text == "") ||
                (celularMaskedTextBox.Text == "") || (emailTextBox.Text == "") || (enderecoTextBox.Text == ""))
            {
                nomeLabel.Font = new Font(nomeLabel.Font, FontStyle.Bold);
                CpfLabel.Font = new Font(CpfLabel.Font, FontStyle.Bold);
                nascimentoLabel.Font = new Font(nascimentoLabel.Font, FontStyle.Bold);
                emailLabel.Font = new Font(emailLabel.Font, FontStyle.Bold);
                celularLabel.Font = new Font(celularLabel.Font, FontStyle.Bold);
                enderecoLabel.Font = new Font(enderecoLabel.Font, FontStyle.Bold);

                return false;
            }
            else
                return true;
          
        }

        public static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);

        }

        public string RemoverEspacos(string texto)
        {
            string nomeTratado = texto.Trim();
            nomeTratado = Regex.Replace(nomeTratado, @"\s+", " ");
            return nomeTratado;
        }

    }

}

