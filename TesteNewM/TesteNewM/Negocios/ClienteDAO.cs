﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TesteNewM.Mapeamento;
using Dapper;
using TesteNewM.Database;

namespace TesteNewM.Negocios
{
    public class ClienteDAO : Conexao
    {
        public bool Criar(Cliente cliente)
        {
            int qtde = 0;
            try
            {
                qtde = conn.Execute(" insert into Clientes (Nome, Nascimento, CPF, Celular, Email, " +
                    "Endereco, Observacao) values (@NOME, @NASCIMENTO, @CPF, @CELULAR, @EMAIL, @ENDERECO, @OBSERVACAO)",
                    new
                    {
                        NOME = cliente.Nome,
                        NASCIMENTO = cliente.Nascimento,
                        CPF = cliente.CPF,
                        CELULAR = cliente.Celular,
                        EMAIL = cliente.Email,
                        ENDERECO = cliente.Endereco,
                        OBSERVACAO = cliente.Observacao,
                        ID = cliente.ClientesID
                    });

            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }

            return qtde == 1;
        }

        public List<Cliente> Listar()
        {
            try
            {
                return conn.Query<Cliente>(
                    " select top 10 ClientesID, Nome, Nascimento, CPF, Celular, Email, " +
                    "Endereco, Observacao from  Clientes ").ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }

        }

        public List<Cliente> ListarPorNomeOuEmail(string texto)
        {
            try
            {
                return conn.Query<Cliente>(
                    " select top 10 ClientesID, Nome, Nascimento, CPF, Celular, Email, Endereco, Observacao" +
                    " from  Clientes where Clientes.Nome like @TEXTO or Clientes.Email like @TEXTO ",
                    new {
                        TEXTO = "%" + texto.Trim() + "%"
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }

        }

        public Cliente procurarPorID(int id)
        {

            Cliente cliente = null;
            try
            {
                cliente = conn.Query<Cliente>(
                    " select ClientesID, Nome, Nascimento, CPF, Celular, Email, " +
                    "Endereco, Observacao from  Clientes where ClientesID = @ID",
                    new
                    {
                        ID = id
                    }).SingleOrDefault();

                return cliente;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }

        }

        public bool Atualizar(Cliente cliente)
        {
          
            int qtd = 0;
            try
            {
                qtd = conn.Execute(" update Clientes set Nome = @NOME, Nascimento = @NASCIMENTO, CPF = @CPF, Celular = @CELULAR, " +
                    "Email = @EMAIL, Endereco = @ENDERECO, Observacao = @OBSERVACAO where ClientesID = @ID",
                    new
                    {
                        NOME = cliente.Nome,
                        NASCIMENTO = cliente.Nascimento,
                        CPF = cliente.CPF,
                        CELULAR = cliente.Celular,
                        EMAIL = cliente.Email,
                        ENDERECO = cliente.Endereco,
                        OBSERVACAO = cliente.Observacao,
                        ID = cliente.ClientesID
                    });
            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }
            return qtd == 1;
        }

        public bool Excluir(int id)
        {

            int qtd = 0;
            try
            {
                qtd = conn.Execute(" delete Clientes where ClientesID = @ID",
                    new
                    {
                        ID = id
                    });
            }
            catch (Exception ex)
            {
                throw new Exception("Erro: " + ex.Message);
            }
            return qtd == 1;
        }



    }
}
