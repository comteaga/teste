﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteNewM.Mapeamento
{
    public class Cliente
    {
   
        public int ClientesID { get; set; }

        public string Nome { get; set; }

        public DateTime Nascimento { get; set; }

        public string CPF { get; set; }

        public string Celular { get; set; }

        public string Email{ get; set; }

        public string Endereco { get; set; }

        public string Observacao { get; set; }

    }
}
