﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TesteNewM.Negocios;

namespace TesteNewM
{
    public partial class ListagemForm : Form
    {
        public ListagemForm()
        {
            InitializeComponent();
            CarregarCadastros();
        }

        private void novoButton_Click(object sender, EventArgs e)
        {
            var cadastro = new CadastroDeClientesForm();

            cadastro.ShowDialog();
            CarregarCadastros();
        }

        private void editarButton_Click(object sender, EventArgs e)
        {
            int idSelecionado = Convert.ToInt32(clientesDataGridView.Rows[clientesDataGridView.CurrentCellAddress.Y].Cells[0].Value.ToString());

            var cadastro = new CadastroDeClientesForm(idSelecionado);

            cadastro.ShowDialog();
            CarregarCadastros();
        }

        private void clientesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editarButton.Enabled = true;
        }

        private void excluirButton_Click(object sender, EventArgs e)
        {
            int idSelecionado = Convert.ToInt32(clientesDataGridView.Rows[clientesDataGridView.CurrentCellAddress.Y].Cells[0].Value.ToString());
            
            var exclusao = new ClienteDAO().Excluir(idSelecionado);

            if (exclusao)
            {
                MessageBox.Show("Cadastro de cliente excluído com sucesso", "Exclusão de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Erro ao Excluir", "Exclusão de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            CarregarCadastros();
        }

        private void CarregarCadastros()
        {
            var listaDeClientes = new ClienteDAO().Listar();
            clientesDataGridView.DataSource = listaDeClientes;
        }

        private void pesquisarButton_Click(object sender, EventArgs e)
        {
            var pesquisa = new ClienteDAO().ListarPorNomeOuEmail(pesquisaTextBox.Text);
            clientesDataGridView.DataSource = pesquisa;
        }

        private void pesquisaTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (pesquisaTextBox.Text.Length == 1)
                CarregarCadastros();
        }
    }
}
