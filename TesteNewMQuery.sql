create database TesteNewMDB

use TesteNewMDB
create table Clientes (
	[ClientesID]	int not null identity(1,1) primary key,
	[Nome]			nvarchar(50) not null,
	[Nascimento]	date not null,
	[CPF]			char(11) not null,
	[Celular]		char(11) not null,
	[Email]			nvarchar(50) not null,
	[Endereco]		nvarchar(100) not null,
	[Observacao]	nText
)

select * from Clientes

